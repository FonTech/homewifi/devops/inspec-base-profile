describe file('/etc/os-release') do
  it { should be_exist }

  its(:content) { should match(/NAME="Debian GNU\/Linux"/) }
  its(:content) { should match(/ID=debian/) }
  its(:content) { should match(/VERSION_ID="9"/) }
end

describe file('/etc/fon') do
  it { should be_exist }
  it { should be_directory }
end

describe file('/etc/fon/metadata.json') do
  it { should be_exist }

  its(:content) { should match(/"ami_version": "\d+\.\d+\.\d+"$/) }
end

# CVE-5754 (Meltdown)
describe package('linux-image-amd64') do
  it { should be_installed }
  its('version') { should eq attribute('kernel_version') }
end
