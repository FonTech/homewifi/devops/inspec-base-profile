describe port(22) do
  its('protocols') { should include 'tcp' }
end

describe service('ssh') do
  it { should be_enabled }
end

describe file('/etc/ssh/auth') do
  it { should be_directory }
  it { should be_mode 0750 }
  it { should be_owned_by 'root' }
end
