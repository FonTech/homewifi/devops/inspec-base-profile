describe service('networking') do
  it { should be_enabled }
end

describe service('rsyslog') do
  it { should be_running }
end
