describe package('puppet-agent') do
  it { should be_installed }
  its('version') { should eq attribute('puppet_agent_version') }
end

describe service('puppet') do
  it { should_not be_enabled }
end

describe file('/etc/puppetlabs/puppet/puppet.conf') do
  it { should exist }
  its(:content) { should match(/server/) }
end
