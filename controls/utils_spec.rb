# To allow the use of "old" networking tools like netstat
describe package('net-tools'), :if => os[:release] =~ /9\.\d/ do
  it { should be_installed }
end

describe package('netcat') do
  it { should be_installed }
end

describe package('curl') do
  it { should be_installed }
end

describe package('lsof') do
  it { should be_installed }
end

describe package('htop') do
  it { should be_installed }
end

describe package('iotop') do
  it { should be_installed }
end
